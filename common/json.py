from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet

class QuerySetEncoder(JSONEncoder):
   def default(self, o):
      if isinstance(o,QuerySet):
         return list(o)
      return super().default(o)


class DateEncoder(JSONEncoder):
   def default(self, o):
      # if o is an instance of datetime
      #    return o.isoformat()
      # otherwise
      #    return super().default(o)
      if isinstance(o, datetime):
         return o.isoformat()
      return super().default(o)



class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
   encoders = {}

   def default(self, o):
      #   if the object to decode is the same class as what's in the
      if isinstance(o, self.model):

      #   model property, then
        #     * create an empty dictionary that will hold the property names
      #       as keys and the property values as values
         d = {}
         # if o has the attribute get_api_url
         #    then add its return value to the dictionary
         #    with the key "href"
         if hasattr(o, 'get_api_url'):
            d["href"] = o.get_api_url()

        #     * for each name in the properties list
        #         * get the value of that property from the model instance
      #           given just the property name
        #         * put it into the dictionary with that property name as
      #           the key
         for name in self.properties:
            val = getattr(o, name)
            if name in self.encoders:
               encoder = self.encoders[name]
               val = encoder.default(val)
            d[name] = val
            d.update(self.get_extra_data(o))

        #     * return the dictionary
         return d
      #   otherwise,
      #       return super().default(o)  # From the documentation
      else:
         return super().default(o)

   def get_extra_data(self, o):
      return {}
